/*
 * Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <climits>
#include <gtest/gtest.h>
#include "../Renderpass2BaseFunc.h"
#include "../ActsRenderpass20019TestSuite.h"
#include "shrinkdefine.h"

using namespace std;
using namespace testing::ext;
using namespace OHOS;

static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64X8d24unormpack32Depthmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.x8_d24_unorm_pack32.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloatDepthmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64S8uintStencilmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.s8_uint.stencil_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duDepthmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duStencilmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint.stencil_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6dusldmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples6duSlStencilmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d16_unorm_s8_uint_separate_layouts.stencil_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uDepthmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uStencilmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint.stencil_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8usldmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D24u8uSlStencilmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d24_unorm_s8_uint_separate_layouts.stencil_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintDepthmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintStencilmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint.stencil_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilnone, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_none*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilnoneunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_none_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilzero, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_zero*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilzerounusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_zero_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldaverage, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_average*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldaverageunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_average_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilmin, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_min*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilminunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_min_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilmax, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_max*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintsldmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.depth_max_unused_resolve*");
static SHRINK_HWTEST_F(ActsRenderpass20019TS, TCImage2d16646Samples64D32sfloats8uintSlStencilmaxunusedresolve, "dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_16_64_6.samples_64.d32_sfloat_s8_uint_separate_layouts.stencil_max_unused_resolve*");